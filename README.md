# A web application to process XRF images

XRF-App is a web application for the analysis of XRF images of cultural heritage works.

XRF stands for X-Ray Fluorescence and is a technique that uses X rays to peek at the chemical components used in the manifucturing of the work being scanned.

This application allows to load an image from a remote digital library and to process it directly in the web browser.

## For developers

The application is written in typescript.

The dependencies are specified in `package.json` and can simply be installed running `npm install` from the project root directory. The dependencies are installed locally in the `node_modules` subdirectory.
